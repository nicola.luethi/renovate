module.exports = {
    endpoint: 'https://git.bedag.cloud/api/v4/',
    platform: 'gitlab',
    persistRepoData: true,
    autodiscover: false,
    includePaths: ["renovate.json"],
    onboarding: false,
    onboardingConfig: {
      extends: ['config:recommended'],
    },
    repositories: ['gelan/gelan-infra/management/renovate-test', 'gelan/gelan-infra/management/system-management'],
    hostRules: [
      {
        matchHost: "artifacts.bedag.cloud",
        token: "${ARTIFACS_TOKEN}"
      },
    ],
    kubernetes: {
      fileMatch: ["\\.yaml$"]
    }
  };